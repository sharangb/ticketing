# Ticketing System

This project implements a simple ticketing system for a bus company with a single bus. It allows the administrator to :
* Close tickets for a customer by providing customer details
* Open ticket and clear customer details
* Get tickets at a glance filtered by status
* View all the details for a single ticket
* Once the ride is complete, reset the system to open up all tickets.


## Local setup

Requires:
* Docker, docker-compose
* Git

Clone this project by running:
```
git clone https://gitlab.com/sharangb/ticketing.git
```
Install the necessary packages:
```
docker-compose -f docker-compose.setup.yml run --rm install
```
Start the services for db and web server:
```
docker stack deploy -c docker-compose.yml ticketing
```

### Postman API test collection

Postman API test collection has been exported to the file `ticketing.postman_collection.json`. The file can be imported into Postman to test the API.
Note that ticketId is a collection variable that must be changed to a relevant ticket ID as the IDs are generated dynamically in MongoDB.

### Other Notes:

Development time: ~10 hours

Learnings & Challenges:
* With no prior experience in Mongoose, understanding models, schemas, static and instance methods
* Setting up mongo with entrypoint script - Creation of user in the right database, setting authentication source database