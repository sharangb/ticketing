db.createUser({
  user: 'systemUser',
  pwd: 'Hx84b_tUiP',
  roles: [{
    role: 'dbOwner',
    db: 'ticketingDb'
  }],
});

tickets = [];
for (let i = 1; i <= 40; i += 1) {
  tickets.push({ seatNumber: i, status: 'OPEN' });
}

db.getSiblingDB('ticketingDb').tickets.insertMany(tickets);