const config = require('config');
const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const mongoose = require('mongoose');

const serverConfig = config.get('server');
const dbConfig = config.get('db');
const logger = require('./util/logger');
const routes = require('./routes');

const app =  express();
const mongoUrl = `mongodb://${dbConfig.user}:${dbConfig.pwd}@${dbConfig.host}:${dbConfig.port}/${dbConfig.name}`;

mongoose.connect(mongoUrl, dbConfig.options);
mongoose.connection.on('error', (error) => {
  logger.error('Error in MongoDb connection: ' + error);
  mongoose.disconnect();
});
mongoose.connection.on('disconnected', () => {
  logger.warn('MongoDB disconnected!');
  mongoose.connect(mongoUrl, dbConfig.options);
});
mongoose.connection.on('connected', () => logger.info('MongoDB connected!'));

app.use(bodyParser.json());
app.use('/', routes);

http.createServer(app)
  .listen(serverConfig.port, (err) => {
    if (err) logger.fatal('Error while starting server:', err);
    else logger.info('Server started on port:', serverConfig.port);
  });

process.on('unhandledRejection', (cause) => {
  logger.error('Unhandled Rejection: ', cause);
});