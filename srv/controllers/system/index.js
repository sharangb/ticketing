const { messages, statusCodes } = require('../../util/constants');
const logger = require('../../util/logger');
const Ticket = require('../../models/ticket');

async function checkHealth(req, res) {
  res.status(statusCodes.success).send({
    message: messages.system.checkHealth.success,
  });
}

async function reset(req, res) {
  logger.info('Resetting the system');
  await Ticket.openAll();
  res.status(statusCodes.success).send({
    message: messages.system.reset.success,
  });
}

module.exports = { checkHealth, reset };