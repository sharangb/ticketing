const { messages, statusCodes } = require('../../util/constants');
const logger = require('../../util/logger');
const Ticket = require('../../models/ticket');

async function getAll(req, res) {
  logger.info('Fetching list of tickets');
  const filter = req.query.status ? { status: req.query.status } : {}; 
  const tickets = await Ticket.find(filter, { _id: 1, seatNumber: 1, status: 1 });
  res.status(statusCodes.success).send({
    message: messages.ticket.getAll.success,
    data: tickets,
  });
}

async function getOne(req, res) {
  logger.info('Fetching ticket details:', req.params.ticketId);
  const ticket = await Ticket.findOne({ _id: req.params.ticketId });
  if (ticket) {
    res.status(statusCodes.success).send({
      message: messages.ticket.getOne.success,
      data: ticket,
    });
  } else {
    res.status(statusCodes.notFound).send({
      message: messages.ticket.getOne.notFound,
    });
  }
}

async function open(req, res) {
  try {
    logger.info('Opening ticket:', req.params.ticketId);
    const ticket = await Ticket.findOne({ _id: req.params.ticketId });
    if (ticket) {
      await ticket.open();
      res.status(statusCodes.success).send({
        message: messages.ticket.open.success,
        data: ticket,
      });
    } else {
      res.status(statusCodes.notFound).send({
        message: messages.ticket.open.notFound,
      });
    }
  } catch(err) {
    logger.info('Error opening ticket:', err.message);
    res.status(statusCodes.internalError).send({
      message: err.message,
    });
  }
}

async function close(req, res) {
  try {
    logger.info('Closing ticket:', req.params.ticketId);
    const ticket = await Ticket.findOne({ _id: req.params.ticketId });
    if (ticket) {
      await ticket.close(req.body);
      res.status(statusCodes.success).send({
        message: messages.ticket.close.success,
        data: ticket,
      });
    } else {
      res.status(statusCodes.notFound).send({
        message: messages.ticket.close.notFound,
      });
    }
  } catch(err) {
    logger.info('Error closing ticket:', err.message);
    res.status(statusCodes.internalError).send({
      message: err.message,
    });
  }
}

module.exports = { getAll, getOne, open, close};