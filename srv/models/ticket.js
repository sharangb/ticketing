const { Schema, model } = require('mongoose');

const { types } = require('../util/constants');

const schema = new Schema({
  seatNumber: {
    type: Number,
    immutable: true,
  },
  status: {
    type: String,
    enum: Object.values(types.ticketStatus),
    default: types.ticketStatus.open,
  },
  user: new Schema({
    name: String,
    age: Number,
    gender: String,
  }),
});

schema.methods = {
  ...schema.methods,
  open: function () {
    this.status = types.ticketStatus.open;
    this.user = {};
    return this.save();
  },
  close: function (user) {
    this.status = types.ticketStatus.closed;
    this.user = user;
    return this.save();
  }
}

schema.statics = {
  ...schema.statics,
  getAllByStatus: function (status) {
    return this.find({ status });
  },
  openAll: function() {
    return this.updateMany({ status: types.ticketStatus.open });
  }
}


module.exports = model('Ticket', schema);
