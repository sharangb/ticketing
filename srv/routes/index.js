const router = require('./router');
const systemRoutes = require('./system');
const ticketRoutes = require('./ticket');

const routes = router.getNew();
routes.use('/api/system', systemRoutes);
routes.use('/api/tickets', ticketRoutes);

module.exports = routes;
