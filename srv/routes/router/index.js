const config = require('config');
const { Router } = require('express');

const routerConfig = config.get('router');

function getNew(options) {
  return Router({ ...routerConfig, ...options});
}

module.exports = {
  getNew: getNew,
};
