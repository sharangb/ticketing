const router = require('./router');
const system = require('../controllers/system');

const routes = router.getNew();
routes.get('/health', system.checkHealth);
routes.get('/reset', system.reset);

module.exports = routes;
