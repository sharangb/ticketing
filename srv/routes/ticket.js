const router = require('./router');
const ticket = require('../controllers/ticket');

const routes = router.getNew();
routes.get('/', ticket.getAll);
routes.get('/:ticketId', ticket.getOne);
routes.get('/:ticketId/open', ticket.open);
routes.patch('/:ticketId/close', ticket.close);

module.exports = routes;
