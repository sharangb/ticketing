module.exports = {
  statusCodes: {
    success: 200,
    notFound: 404,
    internalError: 500,
  },
  messages: {
    ticket: {
      getAll: {
        success: 'Successfully fetched all ticket details',
      },
      getOne: {
        success: 'Successfully fetched ticket details',
        notFound: 'No ticket found with the provided ticket ID'
      },
      open: {
        success: 'Successfully opened ticket',
        notFound: 'Could not open ticket - No ticket found with the provided ticket ID'
      },
      close: {
        success: 'Successfully closed ticket',
        notFound: 'Could not close ticket - No ticket found with the provided ticket ID'
      },
    },
    system: {
      checkHealth: {
        success: 'Server is healthy',
      },
      reset: {
        success: 'Successfully reset all tickets',
      },
    },
  },
  types: {
    ticketStatus: {
      open: 'OPEN',
      closed: 'CLOSED',
    }
  }
};
