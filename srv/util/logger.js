const config = require('config');
const log4js = require('log4js');

const loggerConfig = config.get('logger');

log4js.configure({
  appenders: { 'out': { type: 'stdout', layout: { type: 'basic' } } },
  categories: { default: { appenders: ['out'], level: loggerConfig.level } }
});

module.exports = log4js.getLogger();